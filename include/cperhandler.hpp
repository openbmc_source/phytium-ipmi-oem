/*
 * Copyright (c) 2023, Phytium Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
 
#pragma once

#include <iostream>

using oemCmd = uint8_t;

#define CPER_LOG_DIR "/var/log/ras/"
#define TEMP_FAULT_LOG_PATH "/tmp/cper/cper.tmp"
#define TEMP_FAULT_IPMB_LOG_PATH "/tmp/cper-ipmb/cper.tmp"
#define TEMP_CORE_BIST_MAP_PATH "/tmp/core_bist_map/bist.tmp"


#define PACKET_ID_OFFSET 0

// IPMB related variables.
constexpr uint8_t ipmbRetriesCount = 2;
constexpr uint8_t ipmbChannelMask = 0x03;
constexpr uint8_t oemIpmbNetfn = 0x30;
constexpr uint8_t oemIpmbLun = 0x1;
constexpr uint8_t oemIpmbCmd = 0xff;

enum packetID : uint8_t
{
    FIRST_PACKET = 0,
    LAST_PACKET = 0xFF,
};


namespace oem
{
    constexpr oemCmd cmdAddCommonPlatformErrorRecord = 0x30;
    constexpr oemCmd cmdAddProceeosrCoreBistMap = 0x31;
    constexpr oemCmd cmdReadCommonPlatformErrorRecord = 0x3F;
}
