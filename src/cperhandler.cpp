/*
 * Copyright (c) 2023, Phytium Technology Co., Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "cperhandler.hpp"

#include <ipmiblob/crc.hpp>
#include <ipmid/api-types.hpp>
#include <ipmid/api.hpp>
#include <ipmid/handler.hpp>
#include <phosphor-logging/log.hpp>
#include <sdbusplus/asio/connection.hpp>
#include <sdeventplus/source/time.hpp>

#include <filesystem>
#include <fstream>
#include <string>

namespace fs = std::filesystem;

static std::string getUniqueEntryID(std::string prefix)
{
    static time_t prevTs = 0;
    static int index = 0;

    // Get the entry timestamp
    std::time_t curTs = time(0);
    std::tm timeStruct = *std::localtime(&curTs);
    char buf[80];
    // If the timestamp isn't unique, increment the index
    if (curTs == prevTs)
    {
        index++;
    }
    else
    {
        // Otherwise, reset it
        index = 0;
    }
    // Save the timestamp
    prevTs = curTs;
    strftime(buf, sizeof(buf), "%Y-%m-%dT%H:%M:%S.00", &timeStruct);
    std::string uniqueId(buf);
    uniqueId = prefix + uniqueId + std::to_string(index);
    return uniqueId;
}

static int createFile(std::string fileSavePath, const uint8_t* data,
                      size_t size)
{
    fs::path filePath(fileSavePath);

    if (!fs::is_directory(filePath.parent_path()))
    {
        if (fs::exists(filePath.parent_path()))
        {
            fs::remove_all(filePath.parent_path());
        }
        fs::create_directories(filePath.parent_path());
    }

    std::ofstream out(filePath, std::ofstream::binary | std::ofstream::app);
    if (!out.is_open())
    {
        std::cerr << "Can not open ofstream for CPER binary file" << std::endl;
        return -1;
    }

    out.write(reinterpret_cast<const char*>(data), size);
    out.close();

    return 0;
}

static int readFile(std::string tmpPathFile, std::vector<uint8_t>& fileBuf)
{
    std::ifstream ifs;
    ifs.open(tmpPathFile, std::ios_base::binary);
    if (!ifs.is_open())
    {
        std::cerr << "Can not open ifstream for [" << tmpPathFile
                  << "] binary file\n";
        return -1;
    }

    ifs.seekg(0, std::ios::end);
    fileBuf.resize(ifs.tellg());
    ifs.seekg(0);
    ifs.read(reinterpret_cast<char*>(fileBuf.data()), fileBuf.size());
    ifs.close();

    return 0;
}

int handleCperRecord(std::string fileSavePath)
{
    fs::path tmpCperLogId(fs::path(fileSavePath).parent_path() /
                          getUniqueEntryID(""));
    try
    {

        fs::rename(fs::path(fileSavePath), tmpCperLogId);
    }
    catch (fs::filesystem_error& e)
    {
        std::cerr << "Could not copy cperFile: " << e.what() << '\n';
        return -1;
    }

    auto bus = getSdBus();
    try
    {
        sdbusplus::message::message msg =
            bus->new_signal("/xyz/openbmc_project/Ipmi",
                            "xyz.openbmc_project.Ipmi.Host", "CperRasEvent");

        msg.append(tmpCperLogId.c_str());
        msg.signal_send();
    }
    catch (const sdbusplus::exception::exception& e)
    {
        std::cerr << "Failed to send CperRasEvent signal\n";
    }

    return 0;
}

int handleProceeosrCoreBistMap(std::string fileSavePath)
{
    constexpr uint8_t s5000cDieCoreCount = 16;
    constexpr uint8_t singleMapSize = 8;

    std::vector<uint8_t> bistMapBuf;
    if (readFile(fileSavePath, bistMapBuf) < 0)
    {
        std::cerr << "Read Core Bist Map File Fail" << std::endl;
        return -1;
    }

    if (bistMapBuf.size() < singleMapSize)
    {
        std::cerr << "Invalid Proceeosr Core Bist Map Data" << std::endl;
        return -1;
    }

    uint64_t mapCount = *(reinterpret_cast<uint64_t*>(bistMapBuf.data()));
    if (bistMapBuf.size() != (2 * singleMapSize * mapCount + sizeof(mapCount)))
    {
        std::cerr << "Invalid Core Bist Map Count:" << mapCount << std::endl;
        return -1;
    }

    uint64_t* pCoerMap =
        reinterpret_cast<uint64_t*>(&bistMapBuf[sizeof(mapCount)]);
    uint64_t* pBistMap = reinterpret_cast<uint64_t*>(
        &bistMapBuf[singleMapSize * mapCount + sizeof(mapCount)]);

    for (uint64_t dieId = 0; dieId < mapCount; dieId++)
    {
        uint64_t coreBistResults = pCoerMap[dieId] ^ pBistMap[dieId];
        if (coreBistResults)
        {
            for (uint8_t coreId = 0; coreId < s5000cDieCoreCount; coreId++)
            {
                if ((coreBistResults >> coreId) & 0x1)
                {
                    std::cerr
                        << std::string("Proceeosr Die [" +
                                       std::to_string(dieId) + "]: Core " +
                                       std::to_string(coreId) + " Bist Fail!")
                        << std::endl;
                }
            }
        }
        else
        {
            std::cout << std::string("Proceeosr Die [" + std::to_string(dieId) +
                                     "]: All Core Bist Success!")
                      << std::endl;
        }
    }
    return 0;
}

ipmi::RspType<std::vector<uint8_t>>
    oemReceivePacketsHandle(const std::vector<uint8_t>& reqData,
                            std::string fileSavePath,
                            int (*parseHandle)(std::string))
{
    if (reqData[PACKET_ID_OFFSET] == FIRST_PACKET)
    {
        if (fs::exists(fileSavePath))
        {
            fs::remove_all(fileSavePath);
        }

        if (createFile(fileSavePath, &reqData[PACKET_ID_OFFSET + 1],
                       reqData.size() - PACKET_ID_OFFSET - 1) < 0)
        {
            std::cerr << "Create RAS Temp File Fail,Packet ID = "
                      << reqData[PACKET_ID_OFFSET] << std::endl;
            return ipmi::responseUnspecifiedError();
        }
    }
    else if (reqData[PACKET_ID_OFFSET] == LAST_PACKET)
    {
        if (createFile(fileSavePath, &reqData[PACKET_ID_OFFSET + 1],
                       reqData.size() - PACKET_ID_OFFSET - 1) < 0)
        {
            std::cerr << "Create RAS Temp File Fail: Packet ID = "
                      << reqData[PACKET_ID_OFFSET] << std::endl;
            return ipmi::responseUnspecifiedError();
        }

        parseHandle(fileSavePath);
    }
    else
    {
        if (createFile(fileSavePath, &reqData[PACKET_ID_OFFSET + 1],
                       reqData.size() - PACKET_ID_OFFSET - 1) < 0)
        {
            std::cerr << "create RAS File Fail: Packet ID = "
                      << reqData[PACKET_ID_OFFSET] << std::endl;
            return ipmi::responseUnspecifiedError();
        }
    }
    uint16_t crc16 = ipmiblob::generateCrc(reqData);
    return ipmi::responseSuccess(
        std::vector<uint8_t>({static_cast<uint8_t>(crc16 & 0xff),
                              static_cast<uint8_t>(crc16 >> 8)}));
}

/* Add Common Platform Error Record Command */
ipmi::RspType<std::vector<uint8_t>>
    ipmiAddCommonPlatformErrorRecord(const std::vector<uint8_t>& reqData)
{
    return oemReceivePacketsHandle(reqData, TEMP_FAULT_LOG_PATH,
                                   handleCperRecord);
}

/* Add Proceeosr Core Bist Map Command*/
ipmi::RspType<std::vector<uint8_t>>
    ipmiAddProceeosrCoreBistMap(const std::vector<uint8_t>& reqData)
{
    return oemReceivePacketsHandle(reqData, TEMP_CORE_BIST_MAP_PATH,
                                   handleProceeosrCoreBistMap);
}

/* Read Platform Error Record Command*/
ipmi::RspType<std::vector<uint8_t>> ipmiReadCommonPlatformErrorRecord(
    ipmi::Context::ptr ctx, const uint8_t channelIdx, const uint8_t rasType)
{
    // Check if it is an IPMB channel and if the type meets the range.
    if (channelIdx & ipmbChannelMask)
    {
        std::cerr << "The requested data format is incorrect." << std::endl;
        ipmi::responseInvalidFieldRequest();
    }

    std::vector<uint8_t> tmpBuf(0);
    uint8_t retryCount = 0;

    for (int index = 0; index < LAST_PACKET; index++)
    {
        if (retryCount >= ipmbRetriesCount)
        {
            std::cerr << "Retry still failed, exit." << std::endl;
            return ipmi::responseResponseError();
        }

        typedef std::tuple<int, uint8_t, uint8_t, uint8_t, uint8_t,
                           std::vector<uint8_t>>
            ipmbResponse;

        boost::system::error_code ec;
        auto ipmbResData = ctx->bus->yield_method_call<ipmbResponse>(
            ctx->yield, ec, "xyz.openbmc_project.Ipmi.Channel.Ipmb",
            "/xyz/openbmc_project/Ipmi/Channel/Ipmb", "org.openbmc.Ipmb",
            "sendRequest", channelIdx, oemIpmbNetfn, oemIpmbLun, oemIpmbCmd,
            std::vector<uint8_t>({rasType, static_cast<uint8_t>(index)}));
        if (ec)
        {
            std::cerr << "Dbus call execption." << std::endl;
            retryCount++;
            index--;
            continue;
        }

        int status = -1;
        uint8_t netfn = 0, lun = 0, cmd = 0, cc = 0;
        std::vector<uint8_t> receivedData(0);

        std::tie(status, netfn, lun, cmd, cc, receivedData) = ipmbResData;

        // If the status is incorrect or the response data received is
        // incorrect, a resend operation will be performed, and the maximum
        // number of resends will be returned.
        if (status || cc ||
            (receivedData.size() > 0 && receivedData[0] != index &&
             receivedData[0] != LAST_PACKET))
        {
            std::cerr << "Command error, try to resend." << std::endl;
            retryCount++;
            index--;
            continue;
        }

        if ((receivedData.size() == 0) ||
            (index == FIRST_PACKET && receivedData.size() == 1 &&
             receivedData[0] == LAST_PACKET))
        {
            // No CPER error detected
            std::cout << "Scp has no CPER errors" << std::endl;
            return ipmi::responseSuccess();
        }

        uint8_t resIndex = receivedData[0];

        retryCount = 0;
        tmpBuf.insert(tmpBuf.end(), receivedData.begin() + 1,
                      receivedData.end());

        if (resIndex == LAST_PACKET)
        {
            if (fs::exists(TEMP_FAULT_IPMB_LOG_PATH))
            {
                fs::remove_all(TEMP_FAULT_IPMB_LOG_PATH);
            }

            if (createFile(TEMP_FAULT_IPMB_LOG_PATH, tmpBuf.data(),
                           tmpBuf.size()) < 0)
            {
                std::cerr << "Create RAS Temp File Fail:" << std::endl;
                return ipmi::responseUnspecifiedError();
            }

            handleCperRecord(TEMP_FAULT_IPMB_LOG_PATH);

            return ipmi::responseSuccess();
        }
    }

    return ipmi::responseUnspecifiedError();
}

static void register_ras_functions() __attribute__((constructor));
static void register_ras_functions()
{

    ipmi::registerHandler(ipmi::prioOemBase, ipmi::netFnOemSix,
                          oem::cmdAddCommonPlatformErrorRecord,
                          ipmi::Privilege::User,
                          ipmiAddCommonPlatformErrorRecord);

    ipmi::registerHandler(ipmi::prioOemBase, ipmi::netFnOemSix,
                          oem::cmdReadCommonPlatformErrorRecord,
                          ipmi::Privilege::Admin,
                          ipmiReadCommonPlatformErrorRecord);

    ipmi::registerHandler(ipmi::prioOemBase, ipmi::netFnOemSix,
                          oem::cmdAddProceeosrCoreBistMap,
                          ipmi::Privilege::Admin, ipmiAddProceeosrCoreBistMap);

    return;
}
