# phytium-ipmi-oem

#### 介绍

Phytium的自定义IPMI命令，用于处理一些和主机相关通讯的数据。

#### 使用说明

|  NetFn |  CMD |  说明 |
|---|---|---|
|  0x3A |  0x30 |  处理来自Phytium平台RAS事件的错误记录信息，主要为CPER内容 |

|  NetFn |  CMD |  说明 |
|---|---|---|
|  0x3A |  0x40 |  处理Phytium平台UEFI发送的PCI和磁盘设备信息 |

|  NetFn |  CMD |  说明 |
|---|---|---|
|  0x3A |  0x50 |  接收处理来自BIOS发送的度量命令 |
|  0x3A |  0x51 |  获取TCM处理度量命令返回的数据信息 |

|  NetFn |  CMD |  说明 |
|---|---|---|
|  0x3C |  0x30 |  处理来自Phytium平台主机代理程序的数据，包括CPU、内存、网卡以及磁盘信息 |
